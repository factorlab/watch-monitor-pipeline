# Databricks notebook source
# DBTITLE 1,Configs
raw_path = "s3://factorlab-s3-watch-data-prod/event/pending/v01/raw/" # https://factorlab-s3-watch-data-prod.s3.us-east-1.amazonaws.com/sensor/pending/v01/raw/
bronze_path = "s3a://factorlab-databricks-bronze-data/event_data/" 
bronze_checkpoint_path = f"{bronze_path}_checkpoint/"
silver_path = "s3a://factorlab-databricks-silver-data/event_data/"
silver_checkpoint_path = "{}_checkpoint/".format(silver_path)
code_version = "v0.1"

# COMMAND ----------

# DBTITLE 1,Imports
import time
import pyspark.sql.types as pst
import pyspark.sql.functions as psf
from pyspark.sql.functions import lit
from pyspark.sql.utils import AnalysisException

# COMMAND ----------

# dbutils.fs.rm(bronze_path,True)
# dbutils.fs.rm(silver_path,True)

# COMMAND ----------

# DBTITLE 1,Spark Setting
spark.conf.set('spark.sql.caseSensitive', True)
spark.conf.set('spark.databricks.delta.formatCheck.enabled', False)
spark.conf.set("spark.databricks.delta.optimizeWrite.enabled", "true")
spark.conf.set("spark.databricks.delta.autoCompact.enabled", "true")
spark.conf.set("spark.sql.files.maxPartitionBytes", 1024*1024*512)

# COMMAND ----------

# MAGIC %md ## Raw_Bronze Pipeline

# COMMAND ----------

schema_json = {'type': 'struct',
 'fields': [
  {'name': 'EC', 'type': 'long', 'nullable': True, 'metadata': {}},
  {'name': 'EN', 'type': 'string', 'nullable': True, 'metadata': {}},
  {'name': 'ES', 'type': 'long', 'nullable': True, 'metadata': {}},
  {'name': 'ETS', 'type': 'long', 'nullable': True, 'metadata': {}},
  {'name': 'EventId', 'type': 'string', 'nullable': True, 'metadata': {}},
  {'name': 'UID', 'type': 'string', 'nullable': True, 'metadata': {}},
  {'name': 'ZID', 'type': 'long', 'nullable': True, 'metadata': {}},
  {'name': 'fl_Key', 'type': 'string', 'nullable': True, 'metadata': {}},
  {'name': 'DInfo', 'type': {'type': 'struct',
    'fields': [
     {'name': 'AppName', 'type': 'string', 'nullable': True, 'metadata': {}},
     {'name': 'AppState', 'type': 'long', 'nullable': True, 'metadata': {}},
     {'name': 'AppVer', 'type': 'string', 'nullable': True, 'metadata': {}},
     {'name': 'BStatus', 'type': 'long', 'nullable': True, 'metadata': {}},
     {'name': 'Battery', 'type': 'string', 'nullable': True, 'metadata': {}},
     {'name': 'CPU', 'type': 'string', 'nullable': True, 'metadata': {}},
     {'name': 'ConStatus', 'type': 'long', 'nullable': True, 'metadata': {}},
     {'name': 'DeviceOS', 'type': 'string', 'nullable': True, 'metadata': {}},
     {'name': 'Lng', 'type': 'string', 'nullable': True, 'metadata': {}},
     {'name': 'Ltd', 'type': 'string', 'nullable': True, 'metadata': {}},
     {'name': 'Memory', 'type': 'string', 'nullable': True, 'metadata': {}},
     {'name': 'OWN', 'type': 'string', 'nullable': True, 'metadata': {}},
     {'name': 'PID', 'type': 'string', 'nullable': True, 'metadata': {}},
     {'name': 'Storage', 'type': 'string', 'nullable': True, 'metadata': {}},
     {'name': 'UN', 'type': 'string', 'nullable': True, 'metadata': {}}
    ]}, 'nullable': True, 'metadata': {}},
  {'name': 'WInfo', 'type': {'type': 'struct',
    'fields': [
     {'name': 'PhoneConStatus', 'type': 'long', 'nullable': True, 'metadata': {}},
     {'name': 'SensorStatus', 'type': 'long', 'nullable': True, 'metadata': {}},
     {'name': 'SesCurr', 'type': 'long', 'nullable': True, 'metadata': {}},
     {'name': 'SesLast', 'type': 'long', 'nullable': True, 'metadata': {}},
     {'name': 'URateL', 'type': 'long', 'nullable': True, 'metadata': {}},
     {'name': 'URateS', 'type': 'long', 'nullable': True, 'metadata': {}},
     {'name': 'WrkStatus', 'type': 'long', 'nullable': True, 'metadata': {}}
    ]}, 'nullable': True, 'metadata': {}},
  {'name': 'PInfo', 'type': {'type': 'struct',
    'fields': [
     {'name': 'PairingStatus', 'type': 'long', 'nullable': True, 'metadata': {}},
     {'name': 'WatchAppStatus', 'type': 'long', 'nullable': True, 'metadata': {}},
     {'name': 'BTS', 'type': 'long', 'nullable': True, 'metadata': {}},
     {'name': 'BTAUTH', 'type': 'long', 'nullable': True, 'metadata': {}},
     {'name': 'WatchConStatus', 'type': 'long', 'nullable': True, 'metadata': {}},
     {'name': 'WatchInsStatus', 'type': 'long', 'nullable': True, 'metadata': {}}
    ]}, 'nullable': True, 'metadata': {}},
  {'name': 'Sessions', 'type': {'type': 'array', 'elementType': {'type': 'struct',
     'fields': [
      {'name': 'SesEnd', 'type': 'string', 'nullable': True, 'metadata': {}},
      {'name': 'SesID', 'type': 'string', 'nullable': True, 'metadata': {}},
      {'name': 'SesStart', 'type': 'string', 'nullable': True, 'metadata': {}},
      {'name': 'SesStatus', 'type': 'long', 'nullable': True, 'metadata': {}},
      {'name': 'SesStp', 'type': 'long', 'nullable': True, 'metadata': {}},
      {'name': 'SesTotal', 'type': 'long', 'nullable': True, 'metadata': {}},
      {'name': 'SesUp', 'type': 'long', 'nullable': True, 'metadata': {}},
      {'name': 'filesTotal', 'type': 'long', 'nullable': True, 'metadata': {}},
      {'name': 'filesUp', 'type': 'long', 'nullable': True, 'metadata': {}},
      {'name': 'fl_Key', 'type': 'string', 'nullable': True, 'metadata': {}}
     ]}, 'containsNull': True}, 'nullable': True, 'metadata': {}},
  {'name': 'actions', 'type': {'type': 'array', 'elementType': {'type': 'struct',
     'fields': [
      {'name': 'ACode', 'type': 'long', 'nullable': True, 'metadata': {}},
      {'name': 'ADesc', 'type': 'string', 'nullable': True, 'metadata': {}},
      {'name': 'AID', 'type': 'string', 'nullable': True, 'metadata': {}},
      {'name': 'AName', 'type': 'string', 'nullable': True, 'metadata': {}}
     ]}, 'containsNull': True}, 'nullable': True, 'metadata': {}}
 ]}

# COMMAND ----------

streamSchema = pst.StructType.fromJson(schema_json)

# COMMAND ----------

# DBTITLE 1,Pipeline
df = (spark
      .readStream
      .format("cloudFiles")
      .option("cloudFiles.format", "json")
      .option("cloudFiles.includeExistingFiles", "true")
      .option("cloudFiles.validateOptions", "true")
      .option("cloudFiles.region", "us-east-1")
      .option("cloudFiles.fetchParallelism", 100)
      .option("cloudFiles.useNotifications", "true")
      #.option("mergeSchema", True) # this option is removed for disallowing automatic schema evolution
#       .option("multiLine", "true") # this option is removed for the new JSONL formatted data source
      .schema(streamSchema)
      .load(raw_path)
     .withColumn("processing_time", psf.expr("now()"))
     .withColumn("input_file_name", psf.input_file_name())
     .withColumn("date_value", psf.expr("to_date(from_unixtime(ETS/1000))"))
     .withColumn("code_version", psf.lit(code_version))
 )

(df
 .writeStream
 .format("delta")
 .partitionBy("ZID", "date_value") 
 .outputMode("append")
 .option("checkpointLocation", bronze_checkpoint_path)
 .option("path", bronze_path)
 .option("mergeSchema", True)
 .trigger(processingTime="1 minute")  # or set this to whatever makes sense to the data source
 .start() 
)

# COMMAND ----------

# MAGIC %md ## Bronze_Silver Pipeline

# COMMAND ----------

def merge_into_silver(output_df):
  output_df.createOrReplaceGlobalTempView("bronze_output_df")
  try:
    spark.sql(f"""MERGE INTO delta.`{silver_path}` a
      using global_temp.bronze_output_df b
      ON a.EventId = b.EventId and a.session.SesID = b.session.SesID
      when matched then update set *
      when not matched then insert *
      """)
    
  except AnalysisException: # occurs on the very first write when the silver delta table does not exist
    output_df.write.format("delta").save(silver_path) # the data volume is small enough that date partition is not considered here
    
def foreachBatchFunction(batch_df, batch_id):
  batch_df._jdf.sparkSession().sql("set spark.databricks.delta.optimizeWrite.enabled=true")
  batch_df._jdf.sparkSession().sql("set spark.databricks.delta.autoCompact.enabled=true") 
  batch_df.createOrReplaceGlobalTempView("watch_bronze_input_data")
  upsert_df = (spark.sql(f"""select DInfo, EC, EN, ES, ETS, EventId, UID, WInfo, PInfo, ZID, actions, fl_Key, explode(Sessions) AS session, 
    from_unixtime(ETS / 1000) as time_stamp_value,
    to_date(from_unixtime(ETS / 1000)) as date_value,
    now() as processing_time,
    '{code_version}' as code_version
    from global_temp.watch_bronze_input_data
  """)
   .withColumn("order", psf.expr("row_number() over (partition by ZID, fl_Key, EventId, session.SesID order by ETS desc)"))
   .filter("order = 1")
  .drop("order")) # take the latest value of each session within the batch
  
  merge_into_silver(upsert_df)

# COMMAND ----------

(spark.readStream.format("delta").load(bronze_path)
.writeStream
.option("checkpointLocation", silver_checkpoint_path)
.trigger(processingTime="1 minute")
.foreachBatch(foreachBatchFunction)
.start()
)

# COMMAND ----------

